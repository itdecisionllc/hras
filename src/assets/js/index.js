import "../scss/style.scss";

import Vue from 'vue';

new Vue({
	el: '#app',
  	components: {
  		'header-item': require('./components/common/headerSection.vue').default,
  		'footer-item': require('./components/common/footerSection.vue').default,
  	}
})
