const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const PAGES = ['home', 'course'];

module.exports = {
	entry: { main: './src/assets/js/index.js' },
	output: {
	    path: path.resolve(__dirname, 'dist'),
	    filename: 'assets/js/index.js'
	},
	mode: 'production',
	module: {
	    rules: [
	      	{
		        test: /\.js$/,
		        exclude: /node_modules/,
		        use: {
		          	loader: "babel-loader"
		        }
	      	},{
		        test: /\.vue$/,
		        loader: 'vue-loader',
		    },{
	        	test: /\.scss$/,
	        	use:  ['style-loader', MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader']
	      	},{
				test: /\.(gif|png|jpg|jpeg|svg)?$/,
				loader: 'file-loader',
				options: {
					name: 'img/[name].[ext]',
					outputPath: 'assets',
					publicPath: '../',
					esModule : false
				}
			}
	    ]
	},
	plugins: [
	    new MiniCssExtractPlugin({
	      	filename: 'assets/css/main.css',
	    }),
		...PAGES.map((page) => {
			return new HtmlWebpackPlugin({
				inject: false,
				hash: true,
				template: './src/'+ page +'.html',
				filename: page +'.html'
			});
		}),
	    new VueLoaderPlugin()
	],
	resolve: {
	    alias: {
	      	vue: 'vue/dist/vue.js',
			images: path.resolve(__dirname, 'src/assets/img/')
	    }
	}
}
